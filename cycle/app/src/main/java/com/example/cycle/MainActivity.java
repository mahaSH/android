package com.example.cycle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    @Override
    protected void onStart(){
        super.onStart();
        System.out.println("---------------on start:when activity visible from stop state");
    }
    @Override
    protected void onResume(){
        super.onResume();
        System.out.println("---------------on resume:when activity return to fornground");
    }

    @Override
   protected void onRestart(){
        super.onRestart();
       System.out.println("---------------on resume:when activity return to fornground");
   }
    @Override
    protected void onStop()  {
       super.onStop();
        System.out.println("---------------on stop:when activity return to fornground");
    }
    @Override
    protected void onPause(){
        super.onPause();
        System.out.println("---------------on pause:when activity return to fornground");
        Log.d("my tag",">>>>>>>>>>>>>>>>>> this is a message from log>d");
    }

    @Override
    protected void  onDestroy()  {
       super.onDestroy();
        System.out.println("---------------on distroy:when activity return to fornground");
    }
    public void goToSecondActivity(View view){
        startActivity(new Intent(this,SecondActivity.class));
    }
}