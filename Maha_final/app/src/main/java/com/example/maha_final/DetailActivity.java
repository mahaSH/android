package com.example.maha_final;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity implements View.OnClickListener {

    EditText txtAccountNumber, txtOpenDate, txtBalance, txtName, txtFamily, txtPhone, txtSin;
    Button btnAdd, btnFind, btnRemove, btnUpdate, btnSave, btnLoad, btnClear, btnShow;

    ArrayList<Customer> listOfCustomers;
    int sentIndex;
    String method;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        initialize();
        getMyBundle();
        fillTheFields();

    }

    public void initialize() {
        txtAccountNumber = findViewById(R.id.editTextAccountNuo);
        txtOpenDate = findViewById(R.id.editTextopenDate);
        txtBalance = findViewById(R.id.editTextBalance);
        txtName = findViewById(R.id.editTextName);
        txtFamily = findViewById(R.id.editTextFamily);
        txtPhone = findViewById(R.id.editTextPhone);
        txtSin = findViewById(R.id.editTextSIN);

        btnAdd = findViewById(R.id.btnAddDetails);
        btnAdd.setOnClickListener(this);
        btnFind = findViewById(R.id.btnFind);
        btnFind.setOnClickListener(this);
        btnRemove = findViewById(R.id.btnRemove);
        btnRemove.setOnClickListener(this);
        btnUpdate = findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(this);
        btnSave = findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);
        btnLoad = findViewById(R.id.btnLoad);
        btnLoad.setOnClickListener(this);
        btnClear = findViewById(R.id.btnClear);
        btnClear.setOnClickListener(this);
        btnShow = findViewById(R.id.btnShow);
        btnShow.setOnClickListener(this);

        listOfCustomers = new ArrayList<Customer>();
    }

    //GET THE BUNDLE FROM THE MAIN ACTIVITY:
    public void getMyBundle() {
        Bundle bundle = getIntent().getBundleExtra("intentExtra");
        Serializable bundleList = bundle.getSerializable("bundleExtra");
        listOfCustomers = (ArrayList<Customer>) bundleList;
        Intent intent = getIntent();
        method = intent.getStringExtra("methodExtra");
    }

    //FILL THE FIELDS METHOD
    public void fillTheFields() {
        if (method.matches("update")) {
            Intent intent = getIntent();
            sentIndex = intent.getIntExtra("indexExtra", sentIndex);
            Customer selectedCustomer = listOfCustomers.get(sentIndex);
            txtAccountNumber.setText(selectedCustomer.getAccountNumber().toString());
            txtOpenDate.setText(selectedCustomer.getopenDate().toString());
            txtBalance.setText(selectedCustomer.getBalance() + "");
            txtName.setText(selectedCustomer.getName().toString());
            txtFamily.setText(selectedCustomer.getFamily().toString());
            txtPhone.setText(selectedCustomer.getPhone().toString());
            txtSin.setText(selectedCustomer.getSin().toString());
        }

    }

    //ADD CUSTOMER TO THE LIST METHOD:
    public void addToList() {
        boolean fieldNotEmpty = checkAllFileds();
        if (fieldNotEmpty) {
            String accountNumber, openDate, name, family, phone, sin;
            double balance;

            accountNumber = txtAccountNumber.getText().toString();
            openDate = txtOpenDate.getText().toString();
            name = txtName.getText().toString();
            family = txtFamily.getText().toString();
            phone = txtPhone.getText().toString();
            sin = txtSin.getText().toString();
            balance = Double.valueOf(txtBalance.getText().toString());

            Customer newCustomer = new Customer(accountNumber, openDate, balance, name, family, phone, sin);
            listOfCustomers.add(newCustomer);

            Toast.makeText(this, "Customer Added Successfully", Toast.LENGTH_SHORT).show();
            clearAllFields();
        } else {

            Toast.makeText(this, "All fields should be filled out", Toast.LENGTH_SHORT).show();
            clearAllFields();
            return;
        }
    }

    //FIND BY SIN NUMBER METHOD:
    public void findBySin() {
        String sin = txtSin.getText().toString();
        for (Customer c : listOfCustomers) {
            if (c.getSin().matches(sin)) {
                txtAccountNumber.setText(c.getAccountNumber().toString());
                txtOpenDate.setText(c.getopenDate().toString());
                txtBalance.setText(c.getBalance() + "");
                txtName.setText(c.getName().toString());
                txtFamily.setText(c.getFamily().toString());
                txtPhone.setText(c.getPhone().toString());
                txtSin.setText(c.getSin().toString());
            }
        }
    }

    //REMOVE A CUSTOMER BY SIN
    public void removeBySin() {
        boolean found = false;
        String sin = txtSin.getText().toString();
        for (Customer c : listOfCustomers) {
            if (c.getSin().matches(sin)) {
                listOfCustomers.remove(c);
                Toast.makeText(this, "Customer removed successfully", Toast.LENGTH_SHORT).show();
                found = true;
                clearAllFields();
                return;
            }
        }
        if (found == false) {
            Toast.makeText(this, "No such customer with this SIN", Toast.LENGTH_SHORT).show();
        }
    }

    //UPDATE A CUSTOMER BY SIN
    public void updateBySin() {
        boolean fieldNotEmpty = checkAllFileds();
        boolean found = false;
        if (fieldNotEmpty) {
            String sin = txtSin.getText().toString();
            for (Customer c : listOfCustomers) {
                if (c.getSin().matches(sin)) {
                    c.setAccountNumber(txtAccountNumber.getText().toString());
                    c.setopenDate(txtOpenDate.getText().toString());
                    c.setName(txtName.getText().toString());
                    c.setFamily(txtFamily.getText().toString());
                    c.setPhone(txtPhone.getText().toString());
                    c.setBalance(Double.valueOf(txtBalance.getText().toString()));
                    Toast.makeText(this, "Customer updated successfully", Toast.LENGTH_SHORT).show();
                    found = true;
                    clearAllFields();
                }
                if (found == false) {
                    Toast.makeText(this, "No such customer with this SIN", Toast.LENGTH_SHORT).show();
                }
            }
        } else {

            Toast.makeText(this, "All fields should be filled out", Toast.LENGTH_SHORT).show();
            return;
        }
    }

    //GO BACK TO THE MAIN ACTIVITY:
    public void goToMainActivity() {
        Bundle bundle = new Bundle();
        bundle.putSerializable("bundleExtra", listOfCustomers);
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("intentExtra", bundle);
        setResult(RESULT_OK, intent);
        finish();
    }

    //CLEAR ALL FILEDS METHOD:
    public void clearAllFields() {
        txtAccountNumber.setText("");
        txtOpenDate.setText("");
        txtBalance.setText("");
        txtName.setText("");
        txtFamily.setText("");
        txtPhone.setText("");
        txtSin.setText("");
    }

    //CHECK IF ALL THE FIELDS ARE FILLED OUT METHOD
    public boolean checkAllFileds() {
        String accountNumber, openDate, name, family, phone, sin, balance;

        accountNumber = txtAccountNumber.getText().toString();
        openDate = txtOpenDate.getText().toString();
        name = txtName.getText().toString();
        family = txtFamily.getText().toString();
        phone = txtPhone.getText().toString();
        sin = txtSin.getText().toString();
        balance = txtBalance.getText().toString();
        if (accountNumber.isEmpty() || openDate.isEmpty() || name.isEmpty() || family.isEmpty() || phone.isEmpty() || sin.isEmpty() || balance.isEmpty()) {
            return false;
        }
        return true;
    }

    //OVERRIDDEN METHOD
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAddDetails:
                addToList();
                break;
            case R.id.btnFind:
                findBySin();
                break;
            case R.id.btnRemove:
                removeBySin();
                break;
            case R.id.btnUpdate:
                updateBySin();
                break;
            case R.id.btnClear:
                clearAllFields();
                break;
            case R.id.btnShow:
                goToMainActivity();
                break;
        }

    }
}