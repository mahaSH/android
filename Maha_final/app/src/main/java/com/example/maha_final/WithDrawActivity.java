package com.example.maha_final;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;

public class WithDrawActivity extends AppCompatActivity implements View.OnClickListener {

    EditText edittextWithdraw;
    TextView txtAmount;

    Button btnWithdrow;

    ArrayList<Customer> listOfCustomers;
    int customerIndex;
    Customer selectedCustomer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_with_draw);
        initialize();
        getMyIntent();


    }

    //INITIALIZE METHOD
    public void initialize() {
        edittextWithdraw = findViewById(R.id.editTextwithDrow);
        txtAmount = findViewById(R.id.editTextAmount);

        btnWithdrow = findViewById(R.id.btnwithdraw);
        btnWithdrow.setOnClickListener(this);

        listOfCustomers = new ArrayList<Customer>();
    }

    //GET MY INTENT METHOD
    public void getMyIntent() {
        Bundle bundle = getIntent().getBundleExtra("intentExtra");
        Serializable bundleList = bundle.getSerializable("bundleExtra");
        listOfCustomers = (ArrayList<Customer>) bundleList;
        Intent intent = getIntent();
        customerIndex = intent.getIntExtra("indexExtra", customerIndex);
        selectedCustomer = listOfCustomers.get(customerIndex);
        txtAmount.setText(selectedCustomer.getBalance() + "");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case (R.id.btnwithdraw): {
                double balance = selectedCustomer.getBalance();
                String w = edittextWithdraw.getText().toString();
                if (!w.isEmpty()) {
                    balance = balance - Double.valueOf(w);
                    txtAmount.setText(balance + "");
                    selectedCustomer.setBalance(balance);
                    edittextWithdraw.setText("");
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("bundleExtra", listOfCustomers);
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.putExtra("intentExtra", bundle);
                    setResult(RESULT_OK, intent);
                    finish();
                }
                break;
            }
        }
    }
}