package com.example.maha_final;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    final static int REQUEST_CODE = 1;
    final static int REQUEST_CODE2 = 2;

    ArrayList<Customer> listOfCustomers;
    ArrayAdapter<Customer> customerAdapter;

    ListView listViewCustomers;
    Button btnAdd;

    int clickedCustomerPosition;
    Customer selectedCustomer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
        initializeModel();
        inlitializeListView();


    }

    //INITIALIZE METHOD
    public void initialize() {
        btnAdd = findViewById(R.id.btnAddMain);
        btnAdd.setOnClickListener(this);
    }

    //inlitializeListView() METHOD
    public void inlitializeListView() {
        listViewCustomers = findViewById(R.id.listView);
        listViewCustomers.setOnItemLongClickListener(this);
        listViewCustomers.setOnItemClickListener(this);

        customerAdapter = new ArrayAdapter<Customer>(this, android.R.layout.simple_expandable_list_item_1, listOfCustomers);
        listViewCustomers.setAdapter(customerAdapter);
    }

    // initializeModel() METHHOD
    public void initializeModel() {
        listOfCustomers = new ArrayList<Customer>();

        Customer c1 = new Customer("ABC", "22/12/2005", 2000, "Jerry", "Smith", "514-555-2222", "1"),
                c2 = new Customer("DEF", "22/03/2015", 20505, "Merry", "Adam", "514-999-2123", "3"),
                c3 = new Customer("GHI", "3/8/2020", 100000, "Samantha", "Neil", "514-348-2683", "2");
        listOfCustomers.add(c1);
        listOfCustomers.add(c2);
        listOfCustomers.add(c3);
        Collections.sort(listOfCustomers);
    }

    //GO TO DETAILS ACTIVITY FUNCTION
    public void goToDetailActivity(String method, int item) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("bundleExtra", listOfCustomers);

        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("intentExtra", bundle);

        intent.putExtra("methodExtra", method);
        if (method == "update")
            intent.putExtra("indexExtra", item);
        startActivityForResult(intent, REQUEST_CODE);
    }

    //GET BACK DATA FROM THE OTHER VIEWS
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE) {
            Bundle bundle = new Bundle();
            bundle = data.getBundleExtra("intentExtra");
            Serializable bundleList = bundle.getSerializable("bundleExtra");
            listOfCustomers = (ArrayList<Customer>) bundleList;
            if (resultCode == RESULT_OK)
                inlitializeListView();

        } else {
            Bundle bundle = new Bundle();
            bundle = data.getBundleExtra("intentExtra");
            Serializable bundleList = bundle.getSerializable("bundleExtra");
            listOfCustomers = (ArrayList<Customer>) bundleList;
            if (resultCode == RESULT_OK)
                inlitializeListView();
        }
    }

    //OVERRIDEN METHODS
    @Override
    public void onClick(View view) {
        goToDetailActivity("add", 0);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        selectedCustomer = listOfCustomers.get(i);
        Bundle bundle = new Bundle();
        bundle.putSerializable("bundleExtra", listOfCustomers);

        Intent intent = new Intent(this, WithDrawActivity.class);
        intent.putExtra("intentExtra", bundle);
        intent.putExtra("indexExtra", i);
        startActivityForResult(intent, REQUEST_CODE2);
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        selectedCustomer = listOfCustomers.get(i);
        goToDetailActivity("update", i);
    }
}