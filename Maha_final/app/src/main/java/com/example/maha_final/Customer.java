package com.example.maha_final;

import java.io.Serializable;

public class Customer implements Serializable,Comparable {
    private String accountNumber;
    public String openDate;
    private double balance;

    private String name;
    private String family;
    private String phone;
    private String sin;

    public Customer(String accountNumber, String openDate, double balance, String name, String family, String phone, String sin) {
        this.accountNumber = accountNumber;
        this.openDate = openDate;
        this.balance = balance;
        this.name = name;
        this.family = family;
        this.phone = phone;
        this.sin = sin;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getopenDate() {
        return openDate;
    }

    public void setopenDate(String openDate) {
        this.openDate = openDate;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSin() {
        return sin;
    }

    public void setSin(String sin) {
        this.sin = sin;
    }

    @Override
    public String toString() {
        return family + ", " + name ;
    }

    @Override
    public int compareTo(Object o) {
        Customer otherObject=(Customer) o;
        return family.compareTo(otherObject.getFamily());
    }
}
