package com.example.listcrud;

import androidx.annotation.NonNull;

import java.util.Comparator;

public class Country implements Comparable {
    private String contryName;
    private String capital;

    public Country(String contryName, String capital) {
        this.contryName = contryName;
        this.capital = capital;
    }

    public Country() {
    }

    public String getContryName() {
        return contryName;
    }

    public void setContryName(String contryName) {
        this.contryName = contryName;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    @Override
    public String toString() {
        return getContryName()+" , "+getCapital();
    }

    @Override
    public int compareTo(@NonNull Object o) {
        Country otherCountry=(Country)o;
        return getContryName().compareTo(otherCountry.getContryName());

    }
}
