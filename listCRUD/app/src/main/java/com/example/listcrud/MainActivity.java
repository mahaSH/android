package com.example.listcrud;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener, DialogInterface.OnClickListener {

    //INITIALIZATION
    EditText editTextCountryName, editTextCapital, editTextSearch;
    Button btnAdd, btnSort;

    ListView listViewCountries;
    ArrayList<Country> listOfCountries;
    ArrayAdapter<Country> countryAdapter;

    AlertDialog alertDialogSingle;
    AlertDialog alertDialogMulti;
    AlertDialog.Builder alertDialogBuilderMulti;
    AlertDialog.Builder alertDialogBuilderSnigle;

    int clickedItemPosition;
    Country selectedCountry;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
        initializeModel();
        initializeListView();
        inializeAlert();


    }

    public void initialize() {
        editTextCountryName = findViewById(R.id.editTextCountryName);
        editTextCapital = findViewById(R.id.editTextCapital);
        editTextSearch = findViewById(R.id.editTextSearch);

        btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);

        btnSort = findViewById(R.id.btnSort);
        btnSort.setOnClickListener(this);

    }

    public void initializeModel() {
        listOfCountries = new ArrayList<Country>();
    }

    public void initializeListView() {
        listViewCountries = findViewById(R.id.listViewCountries);
        listViewCountries.setOnItemClickListener(this);
        listViewCountries.setOnItemLongClickListener(this);

        countryAdapter = new ArrayAdapter<Country>(this, android.R.layout.simple_expandable_list_item_1, listOfCountries);

        listViewCountries.setAdapter(countryAdapter);
    }

    public void inializeAlert() {
        alertDialogBuilderSnigle = new AlertDialog.Builder(this);
        alertDialogBuilderSnigle.setTitle("Sort Type")
                .setCancelable(false)
                .setSingleChoiceItems(new String[]{"Ascending", "Descending"},3, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == 0) {
                            Collections.sort(listOfCountries);

                        } else {
                            Collections.sort(listOfCountries, Collections.reverseOrder());
                        }
                        countryAdapter.notifyDataSetChanged();
                    }

                })
                .setPositiveButton("ok", null);
        alertDialogSingle = alertDialogBuilderSnigle.create();
        //MULTIPLE CHOISE DIALOG ALERT BUILDER
        alertDialogBuilderMulti = new AlertDialog.Builder(this);
        alertDialogBuilderMulti.setTitle("Delete Element")
                .setMessage("Are you sure to delete this item(Y/N)")
                .setPositiveButton("Yes", this)
                .setNegativeButton("No", this);
        alertDialogMulti = alertDialogBuilderMulti.create();
    }

    public void addOrUpdateCountry() {
        String countryName = editTextCountryName.getText().toString();
        String capital = editTextCapital.getText().toString();
        String strButtonText = btnAdd.getText().toString();
        if (strButtonText == "UPDATE") {
            //update country information
            selectedCountry.setContryName(editTextCountryName.getText().toString());
            selectedCountry.setCapital(editTextCapital.getText().toString());
            countryAdapter.notifyDataSetChanged();

            editTextCapital.setText("");
            editTextCountryName.setText("");

            editTextCountryName.requestFocus();
            Toast.makeText(this, "Record Updated Successfully", Toast.LENGTH_LONG).show();

            btnAdd.setText("ADD");
        } else {
            //Add Country
            if (!countryName.isEmpty() && !capital.isEmpty()) {
                Country c = new Country(countryName, capital);
                listOfCountries.add(c);
                countryAdapter.notifyDataSetInvalidated();

                editTextCapital.setText("");
                editTextCountryName.setText("");

                editTextCountryName.requestFocus();
                Toast.makeText(this, "Record Added Successfully", Toast.LENGTH_LONG).show();

            } else {
                Toast.makeText(this, "Country name and capital name should not be empty", Toast.LENGTH_SHORT).show();
                ;

            }

        }
    }

    public void sortCountries() {
        alertDialogSingle.show();
        countryAdapter.notifyDataSetChanged();
    }

    public void searchInList() {
        ArrayList<Country> filteredList = new <Country>ArrayList();

        String strToBeFound = editTextSearch.getText().toString().toLowerCase();
        if (!strToBeFound.isEmpty()) {
            for (Country c : listOfCountries) {
                if (c.getContryName().toLowerCase().contains(strToBeFound) || c.getCapital().toLowerCase().contains(strToBeFound)) {
                    filteredList.add(c);
                }
            }
            countryAdapter = new ArrayAdapter<Country>(this, android.R.layout.simple_expandable_list_item_1, filteredList);
            listViewCountries.setAdapter(countryAdapter);
        } else {
            countryAdapter = new ArrayAdapter<Country>(this, android.R.layout.simple_expandable_list_item_1, listOfCountries);
            listViewCountries.setAdapter(countryAdapter);
        }
    }

    //OVERRIDDEN METHODS
    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                listOfCountries.remove(clickedItemPosition);
                countryAdapter.notifyDataSetChanged();
                break;
            case DialogInterface.BUTTON_NEGATIVE:
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAdd:
                addOrUpdateCountry();
                break;
            case (R.id.btnSort):
                sortCountries();
                break;
            case (R.id.btnSearch):
                searchInList();
                break;

        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        clickedItemPosition = position;
        selectedCountry = listOfCountries.get(position);
        editTextCountryName.setText(selectedCountry.getContryName());
        editTextCapital.setText(selectedCountry.getCapital());
        btnAdd.setText("UPDATE");
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
        clickedItemPosition = position;
        alertDialogBuilderMulti.show();
        return true;
    }
}