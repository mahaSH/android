package com.example.multi;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;

public class activityTwo extends AppCompatActivity {
EditText txt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two);
initialize();
myGetIntent();

    }
    private void initialize(){
        txt=findViewById(R.id.txt);
    }
    private void myGetIntent(){
        float result=getIntent().getFloatExtra("result",0f);
        String  strResult=String.valueOf(result);
txt.setText(strResult);
    }
    public void goBack(View view){
        finish();
    }
}