package com.example.multi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity {

    EditText number1;
    EditText number2;
    RadioGroup radioGroup;
    float result = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }

    public void initialize() {
        number1 = findViewById(R.id.no1);
        number2 = findViewById(R.id.no2);
        radioGroup = findViewById(R.id.radioGroup);
    }

    private void getUserInput() {
        float op1 = Float.valueOf(number1.getText().toString());
        float op2 = Float.valueOf(number1.getText().toString());

        int checkRadio = radioGroup.getCheckedRadioButtonId();
        float result = calculateResult(op1, op2, checkRadio);
        goToResultActivity(result);

    }

    private float calculateResult(float no1, float no2, int checkedBtn) {

        switch (checkedBtn) {
            case R.id.Add:
                result = no1 + no2;
                break;

            case R.id.Subtract:
                result = no1 - no2;
                break;

            case R.id.Multiply:
                result = no1 * no2;
                break;

            case R.id.Devide:
                result = no1 / no2;
                break;

        }
        return result;
    }

    private void goToResultActivity(float result) {
        Intent intent = new Intent(this, activityTwo.class);
        intent.putExtra("result", result);
        System.out.println("---------------------"+result);
        startActivity(intent);
    }


    public void operate(View view) {
int btnId=view.getId();
switch(btnId){
    case R.id.btnShow:
        getUserInput();
    case R.id.btnFinish:
        finish();
        break;

}
    }
}