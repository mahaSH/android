package com.example.button;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button btns;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();

    }
 public void initialize() {
     btns = findViewById(R.id.btn);

//Approach 1:Cretae an object
    View.OnClickListener myOnClickListenerObject = new View.OnClickListener() {
         @Override
         public void onClick(View view) {
             Toast.makeText(MainActivity.this, "Approach1:create and pass an object",
                     Toast.LENGTH_LONG).show();
         }
     };
     btns.setOnClickListener(myOnClickListenerObject);
     //Approach 2:Annynoumus inner class
   btns.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
             Toast.makeText(MainActivity.this, "Approach2:Anonymous Inner class",
                     Toast.LENGTH_LONG).show();
         }
     });
     //Approach 3:Lambda Expression
     btns.setOnClickListener((View view )-> {Toast.makeText(MainActivity.this, "Approach3:Lambda Expression",
             Toast.LENGTH_LONG).show();
     }
     );
    //Approach 4:More common
     btns.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        Toast.makeText(MainActivity.this,
                "Approch4",
                Toast.LENGTH_LONG).show();
    }

}
