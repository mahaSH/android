package com.example.classproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class InternetActivity extends AppCompatActivity implements View.OnClickListener {
    WebView webView;
    Button btnLoad;
    String url="https://www.google.com/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internet); initialize();

    }
    public void initialize(){
        webView=findViewById(R.id.webView);

        btnLoad=findViewById(R.id.btnLoad);
        btnLoad.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        webView.getSettings().setJavaScriptEnabled(true);;
        webView.loadUrl(url);;
        webView.setWebViewClient(new WebViewClient());
    }
    @Override
    public void onBackPressed(){
        if(webView.canGoBack()){
            webView.goBack();
        }else{
            super.onBackPressed();
        }
    }

}