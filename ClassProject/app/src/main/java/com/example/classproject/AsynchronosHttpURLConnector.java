package com.example.classproject;

import android.os.AsyncTask;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class AsynchronosHttpURLConnector extends AsyncTask<Void,Integer,String> {
   private String urlString;
   private HttpURLConnectionPostProcessInterface httpURLConnectionPostProcessInterface;

   private TextView textViewProgress;

    public AsynchronosHttpURLConnector(String urlString, HttpURLConnectionPostProcessInterface httpURLConnectionPostProcessInterface, TextView textViewProgress) {
        this.urlString = urlString;
        this.httpURLConnectionPostProcessInterface = httpURLConnectionPostProcessInterface;
        this.textViewProgress = textViewProgress;
    }

    @Override
    protected String doInBackground(Void... params) {
       String urlConnectionResult="";
        HttpURLConnection  httpURLConnection=null;
        try{
            System.out.println("---------------------->"+urlString);
            httpURLConnection=(HttpURLConnection)new URL(urlString).openConnection();
            InputStream urlConnectionInputStream=httpURLConnection.getInputStream();
            System.out.println("----------------------urlconnectioninputStream>"+urlConnectionInputStream);
            urlConnectionResult=inputStreamToString(urlConnectionInputStream);

        }catch(Exception ex){
            httpURLConnectionPostProcessInterface.failureHandler((ex));;

        }
   return urlConnectionResult;

    }
    public  String inputStreamToString(InputStream inputStream){
        BufferedReader bufferReader=null;
        StringBuilder stringBuilder=new StringBuilder();
        InputStreamReader inputStreamReader=new InputStreamReader(inputStream);

        bufferReader=new BufferedReader(inputStreamReader);
        String oneLine=null;
        int counter=0;
        try{
            while((oneLine=bufferReader.readLine())!=null){
                stringBuilder.append(oneLine);
                publishProgress(counter++);
            }
        }catch(Exception ex){
            httpURLConnectionPostProcessInterface.failureHandler(ex);
        }
        System.out.println("----------------------ResultString>"+stringBuilder.toString());
        return stringBuilder.toString();
    }
    @Override
    protected void onPostExecute(String urlConnectionResultString){
        System.out.println("----------------------onPostExcute>"+urlConnectionResultString);
        httpURLConnectionPostProcessInterface.successHandler(urlConnectionResultString);
    }
    @Override
    protected void onProgressUpdate(Integer... values){
        super.onProgressUpdate(values);;
        textViewProgress.setText("Number of lines in loaded webPage: "+String.valueOf(values[0]));
        textViewProgress.setTextSize(16);
        System.out.println("---------------------->Number of lines in loaded webPage: "+String.valueOf(values[0]));
    }
}
