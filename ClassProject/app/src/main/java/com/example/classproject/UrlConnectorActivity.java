package com.example.classproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

public class UrlConnectorActivity extends AppCompatActivity implements View.OnClickListener, HttpURLConnectionPostProcessInterface {
 TextView textViewData,textViewProgress;
 Button btnLoad;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_url_connector);
        initialize();

    }
    public void initialize(){
       textViewData=findViewById(R.id.textViewData);
       textViewProgress=findViewById(R.id.textViewProgress);


        btnLoad=findViewById(R.id.butnLoadUrl);
        btnLoad.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        String url="https://www.w3schools.com/xml/simple.xml";
         AsynchronosHttpURLConnector connector=new AsynchronosHttpURLConnector (url,this,textViewProgress);
         connector.execute();
    }

    @Override
    public void successHandler(String dataInXML) {
        System.out.println("---------- successHandler"+dataInXML);
    }

    @Override
    public void failureHandler(Exception ex) {
        System.out.println("---------- failHandler");
    }
}