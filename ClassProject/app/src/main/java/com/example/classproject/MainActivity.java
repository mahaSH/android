package com.example.classproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent;
    }
    @Override
    public void onClick(View view){
        switch (view.getId()){
            case(R.id.btnInternet):
                Intent intent = new Intent(this, InternetActivity.class);
                startActivity(intent);
                break;
            case(R.id.btnAsynch):
                Intent intent2 = new Intent(this, UrlConnectorActivity.class);
                startActivity(intent2);
                break;
            case(R.id.btnDialog):
                Intent intent3 = new Intent(this, dialogActivity.class);
                startActivity(intent3);
                break;
        }
    }
}