package com.example.classproject;

public interface HttpURLConnectionPostProcessInterface {
    void successHandler(String dataInXML);
    void failureHandler(Exception ex);
}
