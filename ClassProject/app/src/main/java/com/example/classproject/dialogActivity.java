package com.example.classproject;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class dialogActivity extends AppCompatActivity implements DialogInterface.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);
    }
    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                Toast.makeText(dialogActivity.this,
                        "id= " + which,
                        Toast.LENGTH_LONG).show();
                break;
            case DialogInterface.BUTTON_NEGATIVE:
                Toast.makeText(dialogActivity.this,
                        "id= " + which,
                        Toast.LENGTH_LONG).show();
                break;
            case DialogInterface.BUTTON_NEUTRAL:
                Toast.makeText(dialogActivity.this,
                        "id= " + which,
                        Toast.LENGTH_LONG).show();
                break;

        }
    }
    //Alert Dialog --just a question
    public void showAlertDialog(View view) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("this is my title");
        alertDialog.setPositiveButton("yes", this);
        alertDialog.setNegativeButton("no", this);
        alertDialog.setNeutralButton("Neutral", this);
        AlertDialog alert = alertDialog.create();
        alert.show();


    }


//Alert Dialog --just a question--with icon
    public void showAlertDialog2(View view) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(("Alert Dialog \n just a question for demo"))
                .setMessage("Do you Want to delete this file")
                .setCancelable(false)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(dialogActivity.this, "file deleted", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("No", null)
                .setNeutralButton("cancel", null);
        builder.show();


    }
//Alert Dialog --radio buttons
    public void showAlertDialog3(View view) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Question")
                .setCancelable(false)
                .setSingleChoiceItems(new String[]{"A", "B", "C", "D"}, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(dialogActivity.this, "i= " + i, Toast.LENGTH_LONG).show();

                    }
                })
                .setPositiveButton("ok", null);
        builder.show();


    }
   // Alert Dialog -- checkbox
    public void showAlertdIALOG4(View view) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("")
                .setMultiChoiceItems(new String[]{"item0", "item 1", "item 2", "item 3"},
                        new boolean[]{false, true, false, true},
                        new DialogInterface.OnMultiChoiceClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                                Toast.makeText(dialogActivity.this, "item= " + i + ": " + b, Toast.LENGTH_LONG).show();
                            }
                        })
                .setPositiveButton("ok", null);
        builder.show();
    }
//Alert Dialog --same dialog
    public void customDialog(View view) {
        Dialog d = new Dialog(this);
        d.setContentView(R.layout.activity_dialog);
        d.show();

    }
//Alert Dialog --progress bar
    public void showProgressDialog(View view) {
        Log.d("Tag","----------------------------------------->progress button clicked");
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle("progress dialog example");
        progress.setMessage("please wait...");
        progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progress.show();

        progress.setProgress(0);

        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (progress.getProgress() < progress.getMax()) {
                    progress.incrementProgressBy(1);
                } else {
                    progress.dismiss();
                    this.cancel();
                }
            }
        }, 0, 200);
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (progress.getSecondaryProgress() < progress.getMax()) {
                    progress.incrementSecondaryProgressBy(1);
                }
            }
        }, 0, 140);
    }


}