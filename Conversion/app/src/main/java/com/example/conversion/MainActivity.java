package com.example.conversion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
EditText editLastName;
Button btnTemp,btnMetric,btnFinish;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();

    }
    public void initialize(){
        editLastName=findViewById(R.id.temp);
        btnTemp=findViewById(R.id.btn1);
        btnMetric=findViewById(R.id.btn2);
        btnFinish=findViewById(R.id.btn3);

    }
    public void operate(View view){
        int btnId=view.getId();
        switch(btnId) {

            case R.id.btn1:
                tempratureConversion();
                break;

            case R.id.btn2:
                metricConversion();
                break;

            case R.id.btn3:
                finish();

        }

    }

    public void tempratureConversion(){
        Intent myIntent=new Intent(this,TempratureActivity.class);
        startActivity(myIntent);
    }
    public void metricConversion(){
        String lastName=editLastName.getText().toString();
        Intent myIntent=new  Intent(this,MetricActivity.class);
        myIntent.putExtra("LastName",lastName);
        startActivity(myIntent);

    }
}