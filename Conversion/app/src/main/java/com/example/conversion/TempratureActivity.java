package com.example.conversion;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class TempratureActivity extends AppCompatActivity {
TextView temptxtF;
EditText tempEditC;
Button btnReturn,btnConvert;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temprature);
        initialize();
    }
    public void initialize(){
        tempEditC=findViewById(R.id.tempc);
        temptxtF=findViewById(R.id.tempf);
        btnReturn=findViewById(R.id.btnReturn);
        btnConvert=findViewById(R.id.btnConvert);

    }
    public void tempratureOperate(View view){
        int  btnId=view.getId();
        switch(btnId){
            case R.id.btnConvert:
                buttonConvert();
                break;
            case R.id.btnReturn:
                finish();

        }
    }
    public void buttonConvert(){
        float cls=Float.valueOf(tempEditC.getText().toString());
        double result=((9.0/5.0)*cls)+32;
        temptxtF.setText(String.valueOf(result));
    }
}