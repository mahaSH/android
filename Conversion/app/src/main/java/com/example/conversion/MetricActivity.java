package com.example.conversion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MetricActivity extends AppCompatActivity {
 TextView cmtxt,kmtxt,lastNametxt;
 EditText metericEditText;
 Button btnConvert,btnReturn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metric);
    initialize();
    myGetIntent();


    }
    public void initialize(){
        metericEditText=findViewById(R.id.metric);
        cmtxt=findViewById(R.id.cm);
        kmtxt=findViewById(R.id.km);
        lastNametxt=findViewById(R.id.lastName);
        btnConvert=findViewById(R.id.btnConvert);
        btnReturn=findViewById(R.id.btnReturn);

    }
    public void myGetIntent(){
        Intent intent=getIntent();
        String lastName=intent.getStringExtra("lastName");
        lastNametxt.setText(lastName);
    }
    public void metricConvert(View view){
        int btnId=view.getId();
        switch(btnId){
            case (R.id.btnConvert):
                buttonConvert();
                break;
            case R.id.btnReturn:
                finish();

        }
    }
    public void buttonConvert(){
        double centemeters=Double.valueOf(metericEditText.getText().toString())*100;
        cmtxt.setText(String.valueOf(centemeters));
        double kilometers=Double.valueOf(metericEditText.getText().toString())/1000;
        kmtxt.setText(String.valueOf(kilometers));

    }
}