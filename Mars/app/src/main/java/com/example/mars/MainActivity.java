package com.example.mars;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
RadioGroup radioGroup;
EditText name;
EditText age;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }
    public void initialize(){
radioGroup=findViewById(R.id.radioGroup);
name=findViewById(R.id.txtName);
age=findViewById(R.id.txtAge);
    }
    public void submitData(View view){
        int selectedState=radioGroup.getCheckedRadioButtonId();
        String strState=null;
        switch(selectedState){
            case R.id.married:
             strState="married" ;
             break;
            case R.id.single:
                strState="single";
                break;

        }
        String strName=name.getText().toString();
        String ageStr=age.getText().toString();
        String info="Your name is  "+strName+"\n"+" You are "+ageStr+" Years old "+"\n"+"You Are "+strState;
        Toast.makeText(this,info,Toast.LENGTH_LONG).show();
    }
}
