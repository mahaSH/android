package com.example.practice;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
EditText cake;
EditText cookie;
RadioGroup radioGroup;
ImageView image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();

    }
    public void initialize(){
        cake=findViewById(R.id.cake);
        cookie=findViewById(R.id.txtCookie);
        radioGroup=findViewById(R.id.radioGroup);
        image=findViewById(R.id.imageView);
    }
    public void radioClicked(View view){
        int selectedBtn=radioGroup.getCheckedRadioButtonId();
        if(selectedBtn==R.id.cake){
            image.setImageResource(R.drawable.cake);
            Toast.makeText(this,cake.getText().toString(),Toast.LENGTH_LONG).show();
        }else{
            image.setImageResource(R.drawable.cookies);
            Toast.makeText(this,cookie.getText().toString(),Toast.LENGTH_LONG).show();
        }
    }
}
