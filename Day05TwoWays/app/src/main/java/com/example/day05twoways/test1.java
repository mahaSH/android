package com.example.day05twoways;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class test1 extends AppCompatActivity implements View.OnClickListener, TextWatcher {

    TextView textViewOperation;
    EditText editTextAnswer;
    Button btnGenerate,btnValidate,btnCancel;
    int rightResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test1);
        initialize();

    }
public void initialize(){
        textViewOperation=findViewById(R.id.txtOperation);
        editTextAnswer=findViewById(R.id.txtanswer);
        btnGenerate=findViewById(R.id.btnGenerate);
        btnCancel=findViewById(R.id.btnCancel);
        btnValidate=findViewById(R.id.btnValidate);
}
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
try{
    int userAsnwer=Integer.valueOf(editTextAnswer.getText().toString());
    if(userAsnwer>18){
        Toast toastRange=Toast.makeText(this,"the total should be less than 18",Toast.LENGTH_LONG);
        toastRange.setGravity(Gravity.TOP|Gravity.CENTER,0,500);
        toastRange.show();
        btnValidate.setEnabled(false);

    }else{
        btnValidate.setEnabled(true);
    }
}catch(Exception ex){
    Toast toastException=Toast.makeText(this,"Enter a number data type",Toast.LENGTH_LONG);
    toastException.setGravity(Gravity.TOP|Gravity.CENTER,0,500);
    toastException.show();
}
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnGenerate:
                goGenerate();
                break;
            case R.id.btnValidate:
                goValidate();
                break;
            case R.id.btnCancel:
                goCancel();
                break;
        }
    }
    public void  goGenerate(){
        Random r=new Random();
        int op1=r.nextInt(10);
        int op2=r.nextInt(10);
        rightResult=op1+op2;
        String operation=String.valueOf(op1)+"+"+String.valueOf(op2)+"=?";
        textViewOperation.setText(operation);
    }
    public void goValidate(){
        int userAnswer=Integer.valueOf(editTextAnswer.getText().toString());
        String strResult;
        if(userAnswer==rightResult){
            strResult="Right Answer";

        }else{
            strResult="wrong answer";

        }
        Intent intent =new Intent();
        intent.putExtra("returnResultBack",strResult);
        setResult(RESULT_OK,intent);
        finish();
    }
    public void goCancel(){
        String strResult="operation cancelled";
        Intent intent =new Intent();
        intent.putExtra("cancelTag",strResult);
        setResult(RESULT_CANCELED,intent);
        finish();
    }
}