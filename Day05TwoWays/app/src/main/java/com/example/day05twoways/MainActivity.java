package com.example.day05twoways;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    final static int REQUEST_CODE1 = 1;
    final static int REQUEST_CODE2 = 2;

    TextView feedBacktxt;
    Button btnTest1, btnTest2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();

    }

    public void initialize() {
        feedBacktxt = findViewById(R.id.text);
        btnTest1 = findViewById(R.id.btntest1);
        btnTest2 = findViewById(R.id.btntest2);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btntest1:
                gotoTest1();
                break;
            case R.id.btntest2:
                gotoTest2();
                break;
        }
    }

    public void gotoTest1() {
        Intent intent = new Intent(this, test1.class);
        startActivityForResult(intent, REQUEST_CODE1);

    }

    public void gotoTest2() {
        Intent intent = new Intent(this, test2.class);
        startActivityForResult(intent, REQUEST_CODE2);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE1) {
            String receivedDate = (String) data.getStringExtra("returnResultBack");
            if (resultCode == RESULT_OK)
                feedBacktxt.setText(receivedDate);
            else
                feedBacktxt.setText("Cancelled");
        }
        if (requestCode == REQUEST_CODE2) {
            String receivedDate = (String) data.getStringExtra("returnResultFromTest2");
            if (resultCode == RESULT_OK)
                feedBacktxt.setText(receivedDate);
            else
                feedBacktxt.setText("Cancelled");
        }
    }
}