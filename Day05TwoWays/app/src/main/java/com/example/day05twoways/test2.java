package com.example.day05twoways;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class test2 extends AppCompatActivity implements View.OnClickListener {
    Button btnOkTest2,btnCancelTest2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test2);
        initialize();

    }
    public void initialize(){
        btnOkTest2=findViewById(R.id.btntest2Ok);
        btnOkTest2.setOnClickListener(this);
        btnCancelTest2=findViewById(R.id.btntest2Cancel);
        btnCancelTest2.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btntest2Ok:
                goOk();
                break;
            case R.id.btntest2Cancel:
                goCancel();
                break;
        }
    }
    public void goOk(){
        String strResult="OK result from test2";
        Intent intent =new Intent();
        intent.putExtra("returnResultFromTest2",strResult);
        setResult(RESULT_OK,intent);
        finish();

    }
    public void goCancel(){
        String strResult="Operation cancelled";
        Intent intent =new Intent();
        intent.putExtra("cancelTag",strResult);
        setResult(RESULT_CANCELED,intent);
        finish();

    }
}