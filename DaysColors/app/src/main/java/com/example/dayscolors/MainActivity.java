package com.example.dayscolors;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {
Spinner spinnerDays;
Button btnAssigColors,btnDisplaySelectedTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();

    }
public void initialize(){
      spinnerDays=findViewById(R.id.spinnerDays);
      spinnerDays.setOnItemSelectedListener(this);;

      btnAssigColors=findViewById(R.id.btnAssign);
      btnAssigColors.setOnClickListener(this);;

      btnDisplaySelectedTextView=findViewById(R.id.btnDisplay);
      btnDisplaySelectedTextView.setOnClickListener(this);;

    }
    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btnDisplay:
                View selectedView=spinnerDays.getSelectedView();;

                TextView selectedTextView=(TextView)selectedView;
                String selectedText=selectedTextView.getText().toString();
                Toast.makeText(this, "You Selected: "+selectedText, Toast.LENGTH_SHORT).show();
                break;
                //----------------
            case R.id.btnAssign:
                ArrayAdapter adapter=ArrayAdapter.createFromResource(
                        this,
                        R.array.myColors,
                        R.layout.support_simple_spinner_dropdown_item


                );
                spinnerDays.setAdapter(adapter);
                break;

        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
TextView textView=(TextView)view;
String selectedTextView=textView.getText().toString();
Toast.makeText(this,"The selected text is: "+selectedTextView+"\n position is "+i+"\n row is "+l,Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}