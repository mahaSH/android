package com.example.mycalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class ResultActivity extends AppCompatActivity implements View.OnClickListener {
    ListView txtQuiz;
    EditText txtRegister;
    TextView txtScore;
    RadioGroup radioGroup;
    RadioButton radioAll;

    double score;
    String name;

    ArrayList<Quiz> listOfQuiz = new ArrayList<Quiz>();
    ArrayList<Quiz> filteredList = new ArrayList<Quiz>();
    ArrayList<String> emptyList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        initialize();
        getMyIntent();
    }

    //INITIALIZATION METHOD
    public void initialize() {

        txtQuiz = findViewById(R.id.listView);
        txtRegister = findViewById(R.id.txtRegister);
        txtScore = findViewById(R.id.txtScore);
        radioGroup = findViewById(R.id.radioGroup);
        radioAll = findViewById(R.id.radioAll);
        //"ALL" RADIO BUTTON IS PRESELECTED IN THE FIRST SHOW
        radioAll.setChecked(true);
    }

    //GETTING SENT INTENT FROM THE MAIN ACTIVITY
    public void getMyIntent() {
        Bundle bundle = getIntent().getBundleExtra("intentExtra");
        Serializable bundleList = bundle.getSerializable("bundleExtra");
        listOfQuiz = (ArrayList<Quiz>) bundleList;

        ArrayAdapter<Quiz> listOfAdapters = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, this.listOfQuiz);
        txtQuiz.setAdapter(listOfAdapters);
        //GET THE SCORE
        Intent intent=getIntent();
        score=intent.getDoubleExtra("extraScore",score);
        String strScore = Long.toString(Math.round(score));
        txtScore.setText(strScore + "%");
        //GET BACK THE NAME(IF FOUND)
        if(intent.getStringExtra("extraString")!=null);
        txtRegister.setText(intent.getStringExtra("extraString"));
    }
    //IF BACK BUTTON PRESSED
    public void goBack() {
        Intent intent = new Intent();
        name = txtRegister.getText().toString();
        if (!name.matches("")) {
            intent.putExtra("returnResultBack", name);
            intent.putExtra("scoreBack", Double.toString(score));
            setResult(RESULT_OK, intent);
            finish();
        } else {
            setResult(RESULT_CANCELED, intent);
            finish();
        }
    }

    //FILTER THE LIST OF TESTS ACCORDING TO THE SELECTED RADIO BUTTON CHECKED
    public void filter() {
        int checkRadio = radioGroup.getCheckedRadioButtonId();
        switch (checkRadio) {
            case R.id.radioAll:
                getMyIntent();
                break;
            case R.id.radioRight:
                showAnswers(1);
                break;
            case R.id.radioWrong:
                showAnswers(2);
                break;
            case R.id.radioSortA:
                showInOrder(1);
                break;
            case R.id.radioSortD:
                showInOrder(2);
                break;
        }
    }

    //IF SHOW BUTTON PRESSED
    public void showAnswers(int status) {
        filteredList = new ArrayList<Quiz>();
        if (status == 1) {
            for (Quiz q : listOfQuiz) {
                if (q.isCorrect()) {
                    filteredList.add(q);
                }

            }
            if(filteredList.size()==0){
                emptyList.add("You dont have any correct answer");
                ArrayAdapter<String> listOfAdapters = new ArrayAdapter<>(this,
                        android.R.layout.simple_list_item_1, this.emptyList);
                txtQuiz.setAdapter(listOfAdapters);
                return;
            }
        } else {
            for (Quiz q : listOfQuiz) {
                if (!q.isCorrect()) {
                    filteredList.add(q);
                }
            }
            if(filteredList.size()==0){
                emptyList.add("You dont have any wrong answer");
                ArrayAdapter<String> listOfAdapters = new ArrayAdapter<>(this,
                        android.R.layout.simple_list_item_1, this.emptyList);
                txtQuiz.setAdapter(listOfAdapters);
                return;
            }
        }
        ArrayAdapter<Quiz> listOfAdapters = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, this.filteredList);
        txtQuiz.setAdapter(listOfAdapters);
    }

    //ASCENDING OR DESCENDING RADIO BUTTON CHECKED
    public void showInOrder(int order) {

        ArrayList<Integer> listOfAnswers = new ArrayList<Integer>();
        for (Quiz q : filteredList) {
            listOfAnswers.add(Integer.valueOf(q.getAnswer()));
        }
        if (order == 1) {
            //
            Collections.sort(listOfQuiz);
        } else {
            Collections.sort(listOfQuiz, Collections.reverseOrder());
        }
        ArrayAdapter<Quiz> listOfAdapters = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, this.listOfQuiz);
        txtQuiz.setAdapter(listOfAdapters);
    }

    //OVERRIDDEN ONCLICK METHOD
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //BACK BUTTON PRESSED
            case (R.id.btnBack):
                goBack();
                break;
            //SHOW BUTTON PRESSED
            case (R.id.btnShow):
                filter();
                break;
        }
    }

}