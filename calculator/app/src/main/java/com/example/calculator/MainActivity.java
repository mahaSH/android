package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    RadioGroup radioGroup;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
    }

    private void initialize() {
        radioGroup = findViewById(R.id.radioGroup);
        imageView = findViewById(R.id.imageView);
    }

    public void showme(View view) {
        int selectRadioBtn = radioGroup.getCheckedRadioButtonId();
        switch (selectRadioBtn) {
            case R.id.goofy:
                imageView.setImageResource(R.drawable.g);
                break;
            case R.id.donald:
                imageView.setImageResource(R.drawable.d);
                break;
            case R.id.mickey:
                imageView.setImageResource(R.drawable.m);
                break;
        }
    }
}