package com.example.students;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import student.Student;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
EditText idtxt,nametxt,agetxt;
Button btnClear,btnRemove,btnAdd,btnShow;
ArrayList<Student> listOfStudents;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();

    }
    public void initialize(){
        listOfStudents=new ArrayList<Student>();

        idtxt=findViewById(R.id.id);
        nametxt=findViewById(R.id.name);
        agetxt=findViewById(R.id.age);

        btnClear=findViewById(R.id.btnClaer);
        btnClear.setOnClickListener(this);

        btnAdd=findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);

        btnRemove=findViewById(R.id.btnRemove);
        btnRemove.setOnClickListener(this);

        btnShow=findViewById(R.id.btnAll);
        btnShow.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btnClaer:
                clearEditText();
                break;
            case R.id.btnRemove:
                processRemove();
                break;
            case R.id.btnAdd:
               addToTheList();
                break;
            case R.id.btnAll:
                processShowAll();
                break;

        }
    }
    public void clearEditText(){
        idtxt.setText(null);
        nametxt.setText(null);
        agetxt.setText(null);

    }
    public void processRemove(){

    }
    public void addToTheList(){
        int studentId=Integer.valueOf(idtxt.getText().toString());
        String name=nametxt.getText().toString();
        int age=Integer.valueOf(agetxt.getText().toString());

        Student student=new Student(studentId,name,age);
        listOfStudents.add(student);
        Toast.makeText(this,"Added successfully.Array size: "+listOfStudents.size(),Toast.LENGTH_LONG).show();

    }
    public void  processShowAll(){
        Bundle bundle=new Bundle();
        bundle.putSerializable("BundleExtra",listOfStudents);
        Intent intent=new Intent (this,ShowResult.class);
        intent.putExtra("intentExtra",bundle);
        startActivity(intent);
    }
}