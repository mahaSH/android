package com.example.students;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;

import student.Student;

public class ShowResult extends AppCompatActivity {
TextView resulttxt;
ArrayList<Student> listOfStudents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_result);
        initialize();

    }
    public void initialize(){
        resulttxt=findViewById(R.id.list);
        Bundle bundle=getIntent().getBundleExtra("intentExtra");
        Serializable bundleList=bundle.getSerializable("BundleExtra");
        listOfStudents=(ArrayList<Student>) bundleList;
        shoeListOfStudents(listOfStudents);

    }
    private void shoeListOfStudents(ArrayList<Student> list){
        String str="";
        for(Student s:list){
            str=str+s;
        }
        resulttxt.setText(str);
    }
}