package com.example.broadcastreceiver;

import android.content.Context;
import android.util.Log;

import java.util.Date;

public class MyReceiver extends PhoneCallRecieved {
    private static final String TAG=MyReceiver.class.getSimpleName();
    @Override
    protected void onIncomingCallStarted(Context ctx, String number, Date start) {
        Log.i(TAG,"---------------incoming: started<"+number+">");
    }

    @Override
    protected void onOutgoingCallStarted(Context ctx, String number, Date start) {
        Log.i(TAG,"---------------outgoing: started<"+number+">");
    }

    @Override
    protected void onIncomingCallAnswered(Context ctx, String number, Date start) {
        Log.i(TAG,"---------------incoming: answered<"+number+">");
    }

    @Override
    protected void onMissedCall(Context ctx, String number, Date start) {
        Log.i(TAG,"---------------incoming: missed<"+number+">");
    }

    @Override
    protected void onIncomingCallEnded(Context ctx, String number, Date start) {
        Log.i(TAG,"---------------incoming: enede<"+number+">");
    }

    @Override
    protected void onOutGoingCallEnded(Context ctx, String number, Date start) {
        Log.i(TAG,"---------------outgoing: ended<"+number+">");
    }
}
