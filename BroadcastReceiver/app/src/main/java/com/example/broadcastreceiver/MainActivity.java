package com.example.broadcastreceiver;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
private static final int PERMS_REQUEST_CODE=1234;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if( Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1){
            askForPermission();

        }
    }
    private void  askForPermission(){
        if(!hasPermissionForCallEven()){
            String[] perms=new String[]{
                    Manifest.permission.READ_PHONE_STATE,Manifest.permission.READ_CALL_LOG};
            ActivityCompat.requestPermissions(this,perms,PERMS_REQUEST_CODE);
            }
        }
        private boolean hasPermissionForCallEven(){
        return ActivityCompat.checkSelfPermission(this,Manifest.permission.READ_PHONE_STATE)== PackageManager.PERMISSION_GRANTED&&
                ActivityCompat.checkSelfPermission(this,Manifest.permission.READ_CALL_LOG)==PackageManager.PERMISSION_GRANTED;
    }
    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            @NonNull String[] permissions,
            @NonNull int[] grantResults){
        if(requestCode==PERMS_REQUEST_CODE){
            if(grantResults[0]==PackageManager.PERMISSION_GRANTED&&
                    grantResults[1]==PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this,"permission granted",Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(this,"permission denied",Toast.LENGTH_SHORT).show();
            }
        }
    }

}