package com.example.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import java.util.Date;

public abstract class PhoneCallRecieved extends BroadcastReceiver {
    private static final String tag=PhoneCallRecieved.class.getSimpleName();
    private static int lastState= TelephonyManager.CALL_STATE_IDLE;
    private static Date callStatTime;
    public static boolean isIncoming;
    private static String savedNumber;
@Override
    public void onReceive(Context context, Intent intent) {
    TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
    telephony.listen(new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            super.onCallStateChanged(state, incomingNumber);
            ;
            savedNumber = incomingNumber;
        }
    }, PhoneStateListener.LISTEN_CALL_STATE);
    String stateStr = intent.getExtras().getString(TelephonyManager.EXTRA_STATE);
    int state = 0;
    if (TelephonyManager.EXTRA_STATE_IDLE.equals(stateStr)) {
        state = TelephonyManager.CALL_STATE_IDLE;
    } else if (TelephonyManager.EXTRA_STATE_OFFHOOK.equals(stateStr)) {
        state = TelephonyManager.CALL_STATE_OFFHOOK;
    } else if (TelephonyManager.EXTRA_STATE_RINGING.equals(stateStr)) {
        state = TelephonyManager.CALL_STATE_RINGING;
    }
    onCallStateChanched (context, state, savedNumber);
}
   private void onCallStateChanched(Context context,int state,String number){
    if(lastState==state){
        return;
    }
    switch (state){
        case TelephonyManager.CALL_STATE_RINGING:
            isIncoming=true;
            callStatTime=new Date();
            savedNumber=number;
            onIncomingCallStarted(context,savedNumber,callStatTime);
            break;
        case TelephonyManager.CALL_STATE_OFFHOOK:
            if(lastState!=TelephonyManager.CALL_STATE_RINGING){
                isIncoming=false;
                callStatTime= new Date();
                savedNumber=number;
                onOutgoingCallStarted(context,savedNumber,callStatTime);
            }else {
                isIncoming = true;
                callStatTime = new Date();
                savedNumber = number;
                onIncomingCallAnswered(context,savedNumber,callStatTime);
            }
            break;
        case TelephonyManager.CALL_STATE_IDLE:
            if(lastState==TelephonyManager.CALL_STATE_RINGING) {
                onMissedCall(context, savedNumber, callStatTime);
            }
                else if(isIncoming){
                   onIncomingCallEnded(context,savedNumber,callStatTime);

                }else{
                    onOutGoingCallEnded(context,savedNumber,callStatTime);
            }
                break;
        default:
            break;

            }
    lastState=state;
    }
    protected abstract void onIncomingCallStarted(Context ctx,String number,Date start);
    protected abstract void onOutgoingCallStarted(Context ctx,String number,Date start);
    protected abstract void onIncomingCallAnswered(Context ctx,String number,Date start);
    protected abstract void onMissedCall(Context ctx,String number,Date start);
    protected abstract void onIncomingCallEnded(Context ctx,String number,Date start);
    protected abstract void onOutGoingCallEnded(Context ctx,String number,Date start);
   }


