package com.example.rating;

import androidx.annotation.NonNull;

public class MealRating implements Comparable {
    private String mealName;

    private int mealRating;

    public MealRating(String mealName, int mealRating) {
        this.mealName = mealName;
        this.mealRating = mealRating;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public int getMealRating() {
        return mealRating;
    }

    public void setMealRating(int mealRating) {
        this.mealRating = mealRating;
    }

    @Override
    public String toString() {
        return "MealRating{" +
                "mealName='" + mealName + '\'' +
                ", mealRating=" + mealRating +
                '}';
    }

    @Override
    public int compareTo(@NonNull Object o) {
        MealRating otherObject=(MealRating) o;
        return mealName.compareTo(otherObject.getMealName());
    }
}
