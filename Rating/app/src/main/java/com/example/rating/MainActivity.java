package com.example.rating;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class MainActivity extends AppCompatActivity implements View.OnClickListener , AdapterView.OnItemSelectedListener {

    Spinner spinnerMeal;
    ImageView  imageViewMeal;
    RatingBar ratingBarMeal;
    Button btnAdd,btnShowAll;

   String listMeal[]={"Salmon","Poutin","Sushi","Tacos"};
   int mealPicture[]={R.drawable.salmon,R.drawable.poutine,R.drawable.sushi,R.drawable.taco};

   ArrayList<MealRating> listOfMealRating;
   ArrayAdapter<String> mealAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();

    }
public void initialize(){
        listOfMealRating=new ArrayList<>();

        ratingBarMeal=findViewById(R.id.ratingBar);
        //--------------------------
    imageViewMeal=findViewById(R.id.imageView);
     btnAdd=findViewById(R.id.btnAdd);
     btnAdd.setOnClickListener(this);

    btnShowAll=findViewById(R.id.btnShow);
    btnShowAll.setOnClickListener(this);

    //-----------------------------------
    spinnerMeal=findViewById(R.id.spinnerMeal);
    spinnerMeal.setOnItemSelectedListener(this);
    mealAdapter=new ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item,listMeal);
    spinnerMeal.setAdapter(mealAdapter);

    //------------------------------



}
    @Override
    public void onClick(View view) {
switch(view.getId()){
    case R.id.btnAdd:
        addMealRating();
        break;
    case R.id.btnShow:
        showAllRating();
        break;

}
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
int image=mealPicture[i];
imageViewMeal.setImageResource(image);;

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
    public void addMealRating(){
        String meal=spinnerMeal.getSelectedItem().toString();

        int rating=(int) ratingBarMeal.getRating();
        MealRating mealRating=new MealRating(meal,rating);
        listOfMealRating.add(mealRating);
        ratingBarMeal.setRating(0);

    }
    public void showAllRating(){
        Collections.sort(listOfMealRating);
        StringBuilder sb=new StringBuilder("");

        for(MealRating m:listOfMealRating){
            sb.append(m+"\n");

        }
        Toast.makeText(this, sb.toString(), Toast.LENGTH_SHORT).show();
    }
}