package com.example.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
ListView listView;
ArrayList<String> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeModel();
        initialize();

    }
    public void initializeModel(){
        arrayList=new ArrayList<>();

        arrayList.addAll(Arrays.asList("item1,","item2","item3","item4"));


    }
    public void initialize(){
        listView=findViewById(R.id.listView);
        ArrayAdapter<String> stringArrayAdapter=new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                arrayList);
        listView.setAdapter(stringArrayAdapter);
    }
}