package com.example.maha;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class RatingResults extends AppCompatActivity implements View.OnClickListener {

    RadioGroup radioGroup;
    EditText txtRegister;
    ListView listView;
    //------------------
    ArrayList<Food> listOfOrders = new ArrayList<Food>();
    ArrayList<Food> listToShow = new ArrayList<Food>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating_results);
        initialze();
        getMyIntent();
    }

    //INITIALIZE METHOD
    public void initialze() {
        radioGroup = findViewById(R.id.radioGroup);
        txtRegister = findViewById(R.id.txtRegister);
        listView = findViewById(R.id.listView);

    }

    //GETTING THE INTENT
    public void getMyIntent() {
        Bundle bundle = getIntent().getBundleExtra("intentExtra");
        Serializable bundleList = bundle.getSerializable("bundleExtra");
        listOfOrders = (ArrayList<Food>) bundleList;
        //-----------
        showInListView(this.listOfOrders);
    }

    //BACK BUTTON PRESSED:GOE TO THE MAIN ACTIVITY
    public void goBack() {
        Intent intent = new Intent();
        String strToSend = txtRegister.getText().toString();
        if (!strToSend.matches("")) {
            intent.putExtra("returnResultBack", strToSend);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            setResult(RESULT_CANCELED, intent);
            finish();
        }
    }

    //FILTER BY NUMBER OF STARS RADIO BUTTON PRESSED
    public void filterByStars(int star) {
        emptyListView();
        //---------------
        switch (star) {
            case 1:
                for (Food m : listOfOrders) {
                    if (m.getMealRating() == 1 || m.getMealRating() == 0.5) {
                        listToShow.add(m);
                    }
                }
                break;
            case 2:
                for (Food m : listOfOrders) {
                    if (m.getMealRating() == 2 || m.getMealRating() == 1.5) {
                        listToShow.add(m);
                    }
                }
                break;
            case 3:
                for (Food m : listOfOrders) {
                    if (m.getMealRating() == 3 || m.getMealRating() == 2.5) {
                        listToShow.add(m);
                    }
                }
                break;

        }
        //---------------
        ArrayAdapter<Food> listOfAdapters = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, this.listToShow);
        listView.setAdapter(listOfAdapters);
    }

    //FILTER BY ORDER RADIO BUTTON PRESSED
    public void filterByOrder(int order) {
        emptyListView();
        //---------------
        if (order == 1) {
            Collections.sort(listOfOrders);

        } else {
            Collections.sort(listOfOrders, Collections.reverseOrder());

        }
        showInListView(this.listOfOrders);
    }

    //EMPTY THE LIST VIEW
    public void emptyListView() {
        listToShow = new ArrayList<Food>();
        showInListView(this.listOfOrders);
        }

    //SHOWINLISTVIEW FUNCTION
    public void showInListView(ArrayList<Food> list) {
        ArrayAdapter<Food> listOfAdapters = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, list);
        listView.setAdapter(listOfAdapters);
    }
    //OVERRIDDEN ONCLICK METHOD IMPLEMENTAION
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case (R.id.btnBack):
                goBack();
                break;
            case (R.id.radio1star):
                filterByStars(1);
                break;
            case (R.id.radio2stars):
                filterByStars(2);
                break;
            case (R.id.radio3stars):
                filterByStars(3);
                break;
            case (R.id.radioAscending):
                filterByOrder(1);
                break;
            case (R.id.radioDescending):
                filterByOrder(2);
                break;

        }
    }
}