package com.example.maha;

import androidx.annotation.NonNull;

import java.io.Serializable;

public class Food implements Comparable, Serializable {
    private String mealName;

    private double mealRating;

    public Food(String mealName, double mealRating) {
        this.mealName = mealName;
        this.mealRating = mealRating;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public double getMealRating() {
        return mealRating;
    }

    public void setMealRating(double mealRating) {
        this.mealRating = mealRating;
    }

    @Override
    public String toString() {
        return "Customer Order:" +
                 mealName + ',' +
                "mealRating:" + mealRating;
    }

    @Override
    public int compareTo(@NonNull Object o) {
        Food otherObject=(Food) o;
        return mealName.compareTo(otherObject.getMealName());
    }
}
