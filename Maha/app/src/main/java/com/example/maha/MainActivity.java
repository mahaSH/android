package com.example.maha;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    final static int REQUEST_CODE = 1;
    Spinner spinnerMeal;
    ImageView imageViewMeal;
    RatingBar ratingBarMeal;
    TextView title;
    //---------------------------------------
    String listMeal[];
    int mealPicture[];
    //---------------------------------------
    ArrayList<Food> listOfMealRating;
    ArrayAdapter<String> mealAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();

    }

    //INITIALIZE METHOD
    public void initialize() {
        spinnerMeal = findViewById(R.id.spinnerMeal);
        imageViewMeal = findViewById(R.id.imageView);
        ratingBarMeal = findViewById(R.id.ratingBar);
        title = findViewById(R.id.title);
        listOfMealRating = new ArrayList<>();
        //------------------------------------------
        String meals[] = {"Salmon", "Poutin", "Sushi", "Tacos"};
        listMeal = meals;
        int pictures[] = {R.drawable.salmon, R.drawable.poutine, R.drawable.sushi, R.drawable.taco};
        mealPicture = pictures;
        spinnerMeal.setOnItemSelectedListener(this);
        mealAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, listMeal);
        spinnerMeal.setAdapter(mealAdapter);

    }

    //ADD RATING BUTTON PRESSED
    public void addRating() {
        String meal = spinnerMeal.getSelectedItem().toString();

        double rating = (double) ratingBarMeal.getRating();
        Food mealRating = new Food(meal, rating);
        listOfMealRating.add(mealRating);
        ratingBarMeal.setRating(0);
    }

    //SHOW BUTTON PRESSED
    public void showRating() {
        Bundle bundle = new Bundle();
        bundle.putSerializable("bundleExtra", listOfMealRating);

        Intent intent = new Intent(this, RatingResults.class);
        intent.putExtra("intentExtra", bundle);
        startActivityForResult(intent, REQUEST_CODE);
    }

    //GET BACK DATA FROM THE OTHER VIEW
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String receivedDate = (String) data.getStringExtra("returnResultBack");
        if (resultCode == RESULT_OK)
            title.setText("Thank you : " + receivedDate);
    }

    //MEAL BUTTON PRESSED
    public void changeToMeal() {
        String meals[] = {"Salmon", "Poutin", "Sushi", "Tacos"};
        listMeal = meals;
        int pictures[] = {R.drawable.salmon, R.drawable.poutine, R.drawable.sushi, R.drawable.taco};
        mealPicture = pictures;
        spinnerMeal.setOnItemSelectedListener(this);
        mealAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, listMeal);
        spinnerMeal.setAdapter(mealAdapter);
    }

    //SALAD BUTTON PRESSED
    public void changeToSalad() {
        String meals[] = {"Chicken Salad", "Montreal", "Green Salad"};
        listMeal = meals;
        int pictures[] = {R.drawable.chicke, R.drawable.montreal, R.drawable.green};
        mealPicture = pictures;
        spinnerMeal.setOnItemSelectedListener(this);
        mealAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, listMeal);
        spinnerMeal.setAdapter(mealAdapter);
    }


    //OVERRIDDEN ONCLICK METHOD IMPLEMENTATION
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case (R.id.btnAdd):
                addRating();
                break;
            case (R.id.btnShow):
                showRating();
                break;
            case (R.id.btnMeal):
                changeToMeal();
                break;
            case (R.id.btnSalad):
                changeToSalad();
                break;
        }

    }

    //OnItemSelectedListener INTERFACE METHODS IMPLEMENTAION
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        int image = mealPicture[i];
        imageViewMeal.setImageResource(image);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}