package client;

import java.io.Serializable;

public class Client implements Serializable {
    /*enum*/
    public enum Movie{
        Adventure,Actin,Comedy
    }
    /*fields*/
    private int clientNumber;
    private String email;
    private Movie movie;
    /*constructor*/

    public Client() {

    }

    public Client(int clientNumbe, String email, Movie movie) {
        this.clientNumber = clientNumbe;
        this.email = email;
        this.movie = movie;
    }
    /*setters and getters*/

    public int getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(int clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }
    /*ToString*/

    @Override
    public String toString() {
        return  "No:" + clientNumber +
                ",email=" + email +
                ",movie=" + movie +'\n'+'\n';
    }
}
