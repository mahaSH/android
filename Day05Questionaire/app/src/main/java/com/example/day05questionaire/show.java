package com.example.day05questionaire;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;

import client.Client;

public class show extends AppCompatActivity implements View.OnClickListener{
RadioGroup radiogroup;
TextView listTxt;
RadioButton all;
ArrayList<Client> listOfClients;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);
        initialize();

    }
    public void initialize(){
        radiogroup=findViewById(R.id.radioGroup);
        listTxt=findViewById(R.id.txtShow);
        Bundle bundle=getIntent().getBundleExtra("intentextra");
        Serializable bundleList=bundle.getSerializable("bundleExtra");
        listOfClients=(ArrayList<Client>)bundleList;
        showListOfClients(listOfClients);
        all=findViewById(R.id.all);
all.setChecked(true);
    }
    private void showListOfClients(ArrayList<Client> list){
        String str="";
        for(Client c:list){
            str=str+c;
        }
        listTxt.setText(str);

    }

    @Override
    public void onClick(View view) {
        int Id=radiogroup.getCheckedRadioButtonId();
        switch(Id){
            case R.id.Action:
                filterListBy(Client.Movie.Actin);
                break;
            case R.id.Adventure:
                filterListBy(Client.Movie.Adventure);
                break;
            case R.id.comedy:
                filterListBy(Client.Movie.Comedy);
                break;
            case R.id.all:
                showListOfClients(listOfClients);
                break;

        }
    }
    public void filterListBy(Client.Movie type){
       ArrayList<Client> filteredList=new ArrayList<Client>();
       for(Client c:listOfClients){
           if (c.getMovie()==type){
               filteredList.add(c);
           }
       }
       showListOfClients(filteredList);
    }
}