package com.example.day05questionaire;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Iterator;

import client.Client;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    EditText clientNumbertxt, clientemailtxt;
    RadioGroup radioGroup;
    Button btnClear, btnAdd, btnRemove, btnUpdate, btnShow;
    ArrayList<Client> clientsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();

    }

    public void initialize() {
        clientsList = new ArrayList<Client>();
        clientemailtxt = findViewById(R.id.clientEmail);
        clientNumbertxt = findViewById(R.id.clientNumber);
        radioGroup=findViewById(R.id.radioGroup);
        btnClear = findViewById(R.id.btnClaer);
        btnAdd = findViewById(R.id.btnAdd);
        btnRemove = findViewById(R.id.btnRemove);
        btnShow = findViewById(R.id.btnShow);
        btnUpdate = findViewById(R.id.btnUpdate);

    }
    public void clearAllFields(){
        clientemailtxt.setText("");
    }

    @Override
    public void onClick(View view) {
        int btnId = view.getId();
        switch (btnId) {
            case R.id.btnClaer:
                clearEditText();
                break;
            case R.id.btnRemove:
                processRemove();
                break;
            case R.id.btnAdd:
                addToTheList();
                break;
            case R.id.btnShow:
                processShowAll();
                break;
            case R.id.btnUpdate:
                processUpdate();
                break;
        }
    }

    public void clearEditText() {
        clientemailtxt.setText(null);
        clientNumbertxt.setText(null);
        RadioButton actionbtn=(RadioButton)findViewById(R.id.Action);
        actionbtn.setChecked(false);
        RadioButton Adventurebtn=(RadioButton)findViewById(R.id.Action);
        Adventurebtn.setChecked(false);
        RadioButton comedybtn=(RadioButton)findViewById(R.id.Action);
       comedybtn.setChecked(false);
    }

    public void addToTheList() {
        //get all inputs
        int clientnumber = Integer.valueOf(clientNumbertxt.getText().toString());
        String email = clientemailtxt.getText().toString();
        Client.Movie movie = null;
        movie=checkMovie();
        //create a client
        Client client = new Client(clientnumber, email, movie);
        //Add to list
        clientsList.add(client);
        //create toast
        Toast.makeText(this, "Added successfully.Array size: " + clientsList.size(), Toast.LENGTH_LONG).show();
        clearEditText();
    }

    public void processRemove() {
        int clientNumber = Integer.valueOf(clientNumbertxt.getText().toString());
        Iterator<Client> iterator = clientsList.iterator();
        boolean find = false;
        while (!find && iterator.hasNext()) {
            Client c = iterator.next();
            if (c.getClientNumber() == clientNumber) {
                find = true;
                iterator.remove();
            }
        }
        if (find) {
            Toast.makeText(this, "The client with the id: " +
                            clientNumber + " is deleted successfully",
                    Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "The client with the id: " +
                            clientNumber + " Can not be found",
                    Toast.LENGTH_LONG).show();
        }
        clearEditText();
    }

    public void processShowAll() {
        //create and initialize the bundle
        Bundle bundle = new Bundle();
        bundle.putSerializable("bundleExtra", clientsList);
        //create and initialize intent
        Intent intent = new Intent(this, show.class);
        intent.putExtra("intentextra", bundle);
        //start activity
        clearEditText();
        startActivity(intent);

    }

    public void processUpdate() {
        int clientNumber = Integer.valueOf(clientNumbertxt.getText().toString());
        boolean found = false;
        for (Client c : clientsList) {
            if (c.getClientNumber() == clientNumber) {
                found = true;
                c.setEmail(clientemailtxt.getText().toString());
                c.setMovie(checkMovie());

            }
        }
        if (found) {
            Toast.makeText(this, "The client with the id: " +
                            clientNumber + " is updated successfully",
                    Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "The client with the id: " +
                            clientNumber + " Can not be found",
                    Toast.LENGTH_LONG).show();
        }
        clearEditText();
    }

    public Client.Movie checkMovie() {

        Client.Movie movie = null;
        int checkedMovie = radioGroup.getCheckedRadioButtonId();
        switch (checkedMovie) {
            case (R.id.Action):
                movie = Client.Movie.Actin;
                break;
            case (R.id.Adventure):
                movie = Client.Movie.Adventure;
                break;
            case (R.id.comedy):
                movie = Client.Movie.Comedy;
                break;

        }
        return movie;
    }

}