package com.example.starbuzz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.List;

import model.Drinks;

public class foods extends AppCompatActivity {
ImageView photo;
TextView name,Description;
Drinks food;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food);
        initialize();
        getMyIntent();
        populateData(food);

    }
    public void initialize(){
       photo=findViewById(R.id.photo);
       name=findViewById(R.id.name);
       Description=findViewById(R.id.description);

    }
    public void getMyIntent(){
        int foodId=(int)getIntent().getExtras().get("extraFoodId");
        food=Drinks.foods[foodId];

    }
    public void populateData(Drinks Food){
        name.setText(food.getName());
        Description.setText(food.getDescription());
        photo.setImageResource(food.getImageResourceId());
    }
}