package com.example.starbuzz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import model.Drinks;

public class FoodCategory extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_category);
        initializeListView();

    }
    public void  initializeListView(){
        //1-Define the arrayList
        ListView listFood=findViewById(R.id.listFood);
        //2-Define the Adapter as an ArrayAdapter
       ArrayAdapter<Drinks> listAdapter=new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                Drinks.foods);
       //3-Plug in the Adapter
       listFood.setAdapter(listAdapter);
       //4-Create onClickItemClickListener eventListener
        AdapterView.OnItemClickListener itemClickListener=new AdapterView.OnItemClickListener() {
            @Override
            //5-Implement the Overriden function
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(FoodCategory.this,
                       foods.class);
                intent.putExtra("extraFoodId",(int)id);
                startActivity(intent);

            }
        };
        //Apply the onclick method to the listview
        listFood.setOnItemClickListener(itemClickListener);
    }
}