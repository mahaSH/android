package com.example.starbuzz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import model.Drinks;

public class DrinkCategory extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink_category);
        initialize();

    }
    public void initialize(){
        ListView listDrinks=findViewById(R.id.listDrinks);
        ArrayAdapter<Drinks> listAdapter=new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                Drinks.drinks);
                //ArrayAdapter<>(this,android.R.layout.simple_list_item_1,Drink.drinks);
        listDrinks.setAdapter(listAdapter);
        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listAdapterView,
                                    View itemView,
                                    int position,
                                    long id) {
                Intent intent = new Intent(DrinkCategory.this,
                        Drink.class);
                intent.putExtra("extraDrinkId",(int)id);
                startActivity(intent);
            }
        };
        listDrinks.setOnItemClickListener(itemClickListener);
    }
}