package com.example.starbuzz;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import model.Drinks;

public class Drink extends AppCompatActivity {
ImageView photo;
TextView name, description;
Drinks drink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink);
        initialize();
        getMyIntent();
        populateData(drink);

    }
    public void initialize(){
        photo=findViewById(R.id.photo);
        name=findViewById(R.id.name);
        description=findViewById(R.id.description);

    }
    public void getMyIntent(){
        int drinkId=(Integer)getIntent().getExtras().get("extraDrinkId");
        drink=Drinks.drinks[drinkId];
    }
    public void populateData(Drinks drink){
        name.setText(drink.getName());
        description.setText(drink.getDescription());
        photo.setImageResource(drink.getImageResourceId());
        photo.setContentDescription(drink.getName());
    }
}