package model;

import com.example.starbuzz.R;

public class Drinks {

    private String name;
    private String description;
    private int imageResourceId;

    public static final Drinks[] drinks = {
            new Drinks("Latte","Acouple of espresso shots with steamed Milk", R.drawable.latte),
            new Drinks("Capputchino"," espresso hot Milk", R.drawable.cappuccino),
            new Drinks("Filter","Heighest Quality beans roasted and brewed fresh", R.drawable.filter)
    };

    public static final Drinks[] foods = {
            new Drinks("Chicken","yummy chicken", R.drawable.chicken),
            new Drinks("Fish"," yummy fish", R.drawable.fish),
            new Drinks("Beef","Yummy Beef", R.drawable.beef)
    };

    public Drinks(String name, String description, int imageResourceId) {
        this.name = name;
        this.description = description;
        this.imageResourceId = imageResourceId;
    }

        public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }

    public void setImageResourceId(int imageResourceId) {
        this.imageResourceId = imageResourceId;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
