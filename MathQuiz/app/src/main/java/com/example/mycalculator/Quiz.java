package com.example.mycalculator;

import java.io.Serializable;

public class Quiz  implements Serializable, Comparable {
    private String question;
    private String answer;
    private boolean isCorrect;

    public Quiz(String question, String answer, boolean isCorrect) {
        this.question = question;
        this.answer = answer;
        this.isCorrect = isCorrect;
    }

    public Quiz() {
    }

    //GETTERS AND SETTERS
    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setCorrect(boolean correct) {
        isCorrect = correct;
    }

    @Override
    public String toString() {
        if (isCorrect) {
            return question + '=' +
                    answer +
                    ", Correct";
        }
        return question + '=' +
                answer +
                ", Wrong";
    }

    @Override
    public int compareTo(Object o) {
        Quiz otherObject = (Quiz) o;
        //return question.compareTo(otherObject.getQuestion());
        return (otherObject.isCorrect == isCorrect ? 0 : (isCorrect ? -1: 1));
    }

}
