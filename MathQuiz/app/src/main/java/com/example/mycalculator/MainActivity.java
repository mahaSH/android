package com.example.mycalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    final static int REQUEST_CODE = 1;
    TextView question, answer, name;
    Button btnGenerate, btnValidate, btnClear, btnScore, btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btnSign, btnDot;
    double userAnswer, correctAnswer;
    int operand1, operand2;
    boolean userAnswerIsCorrect;
    String receivedName;

    String operation;
    double score;

    ArrayList<Quiz> ListOfQuiz = new ArrayList<Quiz>();
    ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();

    }

    public void initialize() {
        //DEFINE AND INITAILIZE THE TEXTVIEWS FOR QUESTION AND ANSWER
        question = findViewById(R.id.txtQuestion);
        answer = findViewById(R.id.txtAnswer);
        name = findViewById(R.id.txtName);

        //Define  buttons
        btnValidate = findViewById(R.id.btnValidate);
        btnGenerate = findViewById(R.id.btnGenerate);
        btn0 = findViewById(R.id.no0);
        btn1 = findViewById(R.id.no1);
        btn2 = findViewById(R.id.no2);
        btn3 = findViewById(R.id.no3);
        btn4 = findViewById(R.id.no4);
        btn5 = findViewById(R.id.no5);
        btn6 = findViewById(R.id.no6);
        btn7 = findViewById(R.id.no7);
        btn8 = findViewById(R.id.no8);
        btn9 = findViewById(R.id.no9);
        btnDot = findViewById(R.id.dot);
        btnSign = findViewById(R.id.sign);
        btnClear = findViewById(R.id.btnClear);
        btnScore = findViewById(R.id.btnScore);
        btnScore.setEnabled(false);
        disableButtons();
    }

    //OVERRIDDEN ONCLICK METHOD
    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            //GENERATE QUESION BUTTON PRESSED:GENERATE A QUESTION
            case (R.id.btnGenerate):
                generateQuestion();
                enableButtons();
                break;
            //NUMBER BUTTON PRESSED
            case (R.id.no0):
                concatenateToAnswer("0");
                break;
            case (R.id.no1):
                concatenateToAnswer("1");
                break;
            case (R.id.no2):
                concatenateToAnswer("2");
                break;
            case (R.id.no3):
                concatenateToAnswer("3");
                break;
            case (R.id.no4):
                concatenateToAnswer("4");
                break;
            case (R.id.no5):
                concatenateToAnswer("5");
                break;
            case (R.id.no6):
                concatenateToAnswer("6");
                break;
            case (R.id.no7):
                concatenateToAnswer("7");
                break;
            case (R.id.no8):
                concatenateToAnswer("8");
                break;
            case (R.id.no9):
                concatenateToAnswer("9");
                break;
            //DOT BUTTON PRESSED
            case (R.id.dot):
                concatenateToAnswer(".");
                break;
            //- BUTTON PRESSED
            case (R.id.sign):
                concatenateToAnswer("-");
                break;
            //CLEAR BUTTON PRESSED
            case (R.id.btnClear):
                answer.setText("");
                btnValidate.setEnabled(false);
                btnClear.setEnabled(false);
                break;
            //FINISH BUTTON PRESSED
            case (R.id.btnFinish):
                finish();
                break;
            //VALIDATE BUTTON PRESSED
            case (R.id.btnValidate):
                validateAnswer();
                break;
            //SCORE BTUUON PRESSED
            case (R.id.btnScore):
                showScore();
                break;
        }
    }

    //GENERATE QUESTION METHOD
    public void generateQuestion() {
        //INITIALIZE THE RANDOM AND THE ARRAY OF OPERATIONS
        String operations[] = {"+", "-", "/", "*"};
        Random r = new Random();
        //WRITE ON THE ANSWER TEXT FIELD
        answer.setText("Enter Your answer");
        //GENERATE THE RANDOM OPERANDS AND OPERATION
        operand1 = r.nextInt(100);
        operand2 = r.nextInt(100);
        int operationInt = r.nextInt(4);
        //CHECK WHICH OPERATION CHOSEN
        switch (operationInt) {
            case (0):
                operation = "+";
                break;
            case (1):
                operation = "-";
                break;
            case (2):
                operation = "/";
                break;
            case (3):
                operation = "*";
                break;

        }
        //TO AVOID DEVIDING BY ZERO:REGENEARTE THE QUESTION IF THE OPERATION IS DEVISION AND THE SECOND OPERAND IS 0
        if (operationInt == 2 && operand2 == 0) {
            generateQuestion();
        }
        //WRITE THE GENERATED OPERATION ON THE TEXTVIEW
        String strQuestion = operand1 + operations[operationInt] + operand2 + "";
        question.setText(strQuestion);
        btnGenerate.setEnabled(false);


    }

    //CONCATINATE TO THE THE ANSWER TEXTvIEW THE PRESSED BUTTON
    public void concatenateToAnswer(String digit) {
        //FOR FIRST DIGIT TO BE ENTERED:CLEAR THE TEXTFILED FIRST
        if (answer.getText().toString().matches("Enter Your answer")) {
            answer.setText("");
        }
        //GET THE STRING IN THE TEXTFIELD.
        String strAnswer = answer.getText().toString();
        //TO AVOID MULTI ZEROS BEFORE THE DECIMAL POINT
        if (strAnswer.length() > 2 && strAnswer.charAt(0) == '0' && strAnswer.charAt(1) != '.') {
            strAnswer = strAnswer.substring(1, strAnswer.length());
        }
        //ENABLE VLIDATE AND CLEAR BUTTONS
        btnValidate.setEnabled(true);
        btnClear.setEnabled(true);
        //FOR NUMERIC DIGITS(EXCEPT 0)
        if (digit != "-" && digit != "." && digit != "0") {
            //NUMBER BUTTON PRESSED
            if (strAnswer.length() == 1 && strAnswer.charAt(0) == '0') {
                strAnswer = strAnswer.substring(1, strAnswer.length());
            }
            answer.setText(strAnswer + digit);
            //FOR <<DOT>> BUTTON
        } else if (digit == ".") {
            //DOT BUTTON PRESSED
            if (strAnswer.matches("")) {
                answer.setText("0" + digit);
                return;
                //TO AVOID MULTIPLE DOTS
            } else if (!strAnswer.contains("."))
                answer.setText(strAnswer + digit);
            //ZERO BUTTON PRESSED
        } else if (digit == "0") {
            //TO AVOID MUTIPLE ZEROS AT THE BEGINING OF THE ANSWER
            if (strAnswer.length() == 1 && strAnswer.charAt(0) == '0') {
                return;
            } else {
                answer.setText(strAnswer + digit);
            }
        } else {
            //SIGN BUTTON PRESSED
            if (strAnswer.length() == 0 || strAnswer.matches("-")) {
                //TO AVOID SUBMITTING ONLY SIGN WITH NO NUMBER
                btnValidate.setEnabled(false);
            }
            //TOSWITCH THE SIGN FROM - TO + AND VISA VERSA
            if (strAnswer.contains("-")) {
                answer.setText(strAnswer.substring(1, strAnswer.length()));
            } else {
                answer.setText("-" + strAnswer);
            }

        }
    }

    //DISABLE BUTTONS FUNCTION
    public void disableButtons() {
        btnValidate.setEnabled(false);
        btn0.setEnabled(false);
        btn1.setEnabled(false);
        btn2.setEnabled(false);
        btn3.setEnabled(false);
        btn4.setEnabled(false);
        btn5.setEnabled(false);
        btn6.setEnabled(false);
        btn7.setEnabled(false);
        btn8.setEnabled(false);
        btn9.setEnabled(false);
        btnDot.setEnabled(false);
        btnSign.setEnabled(false);
        btnClear.setEnabled(false);
        //
        question.setText("");
        answer.setText("");
    }

    //ENABLE BUTTONS FUNCTION
    public void enableButtons() {
        btn0.setEnabled(true);
        btn1.setEnabled(true);
        btn2.setEnabled(true);
        btn3.setEnabled(true);
        btn4.setEnabled(true);
        btn5.setEnabled(true);
        btn6.setEnabled(true);
        btn7.setEnabled(true);
        btn8.setEnabled(true);
        btn9.setEnabled(true);
        btnDot.setEnabled(true);
        btnSign.setEnabled(true);
    }

    //VALIDATE USER'S ANSWER
    public void validateAnswer() {

        btnScore.setEnabled(true);
        //GET THE CORRECT ANSWER
        switch (operation) {
            case ("+"):
                correctAnswer = operand1 + operand2;
                break;
            case ("-"):
                correctAnswer = operand1 - operand2;
                break;
            case ("*"):
                correctAnswer = operand1 * operand2;
                break;
            case ("/"):
                correctAnswer = (double) operand1 / operand2;
                break;
        }
        //EXTRACT USER'S ANSWER FROM ANSWER TEXT FIELD
        String strUserAnswer = answer.getText().toString();
        //CONVERT THE EXTRACTED VALUE TO A DOUBLE
        if (strUserAnswer.charAt(0) == '-')
            userAnswer = 0 - Double.valueOf(strUserAnswer.substring(1, strUserAnswer.length()));
        else
            userAnswer = Double.valueOf(strUserAnswer);
        //CHECK IF THE USER'S ANSWER MTCHES THE CORRECT ANSWER
        if (operation != "/") {
            if (userAnswer == correctAnswer) {
                Toast.makeText(this, "Good job " + operand1 + operation + operand2 + "=" + correctAnswer, Toast.LENGTH_LONG).show();
                userAnswerIsCorrect = true;
            } else {
                Toast.makeText(this, "Wrong Answer " + operand1 + operation + operand2 + "=" + correctAnswer, Toast.LENGTH_LONG).show();
                userAnswerIsCorrect = false;
            }
        } else {
            //TO AVOID ENDLESS DIGITS IN DIVISION OPERATION
            correctAnswer = Math.floor(correctAnswer * 1000) / 1000;
            userAnswer = Math.floor(userAnswer * 1000) / 1000;
            if (userAnswer == correctAnswer) {
                Toast.makeText(this, "Good job " + operand1 + operation + operand2 + "=" + correctAnswer, Toast.LENGTH_LONG).show();
                userAnswerIsCorrect = true;
            } else {
                Toast.makeText(this, "Wrong Answer " + operand1 + operation + operand2 + "=" + correctAnswer, Toast.LENGTH_LONG).show();
                userAnswerIsCorrect = false;
            }
        }


        //CREATE AN OBJECT AND ADD IT TO THE LIST

        Quiz newQuiz = new Quiz(operand1 + operation + operand2 + "", strUserAnswer, userAnswerIsCorrect);
        ListOfQuiz.add(newQuiz);
        //CALCULATE THE SCORE
        int correct = 0;
        int notCorrect = 0;
        int total = ListOfQuiz.size();


        for (Quiz q : ListOfQuiz) {
            if (q.isCorrect())
                correct = correct + 1;
        }

        score = ((double) correct * 100 / total);
        String strScore = Long.toString(Math.round(score));
        //IF THE USER REGISTERED,UPDATE THE SCORE IN THE UPPER TEXT FIELD
        if (!name.getText().toString().matches("Math Quiz")) {
            name.setText(receivedName + " ,your score is " + strScore + "%");
        }

        //INITIALIZE AND CLEAR INPUTS
        btnGenerate.setEnabled(true);
        disableButtons();
    }


    //CREATE AN INTENT TO SEND THE SCORE TO THE OTHER ACTIVITY
    public void showScore() {
        Bundle bundle = new Bundle();
        bundle.putSerializable("bundleExtra", ListOfQuiz);

        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra("intentExtra", bundle);
        intent.putExtra("extraScore",score);
        if(receivedName!=null){
            intent.putExtra("extraString",receivedName);
        }
        startActivityForResult(intent, REQUEST_CODE);

    }

    //GET BACK DATA FROM THE OTHER ACTIVITY
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            score = Double.valueOf(data.getStringExtra("scoreBack"));
            String strScore = Long.toString(Math.round(score));
            receivedName = (String) data.getStringExtra("returnResultBack");
            name.setText(receivedName + " ,your score is " + strScore + "%");

        }
    }
}