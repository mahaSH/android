package com.mysimpledream.s7_conversion_intent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MetricActivity extends AppCompatActivity {

    TextView centimeterTextViewValue, kilometerTextViewValue, lastNameTextViewValue;
    EditText meterEditText;
    Button convertBtn, returnBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metric);

        initialize();
        myGetIntent();
    }

    private void initialize() {
        meterEditText = findViewById(R.id.meterEditText);

        centimeterTextViewValue = findViewById(R.id.centimeterTextViewValue);
        kilometerTextViewValue = findViewById(R.id.kilometerTextViewValue);
        lastNameTextViewValue = findViewById(R.id.lastNameTextViewValue);

        convertBtn = findViewById(R.id.convertBtn);
        returnBtn = findViewById(R.id.returnBtn);
    }

    private void myGetIntent() {
        //------------------------------------------------ Get Intent
        Intent intent = getIntent();
        String lastName = intent.getStringExtra("lastName");

        //String lastName = getIntent().getStringExtra("lastName");

        lastNameTextViewValue.setText(lastName);
    }


    public void matrixConvert(View view) {

        int btnID = view.getId();

        switch (btnID) {

            case R.id.convertBtn:
                buttonConvert();
                break;
            case R.id.returnBtn:
                finish();
                break;
        }
    }

    private void buttonConvert() {

        double centimeter = Double.valueOf(meterEditText.getText().toString()) * 100;
        centimeterTextViewValue.setText(String.valueOf(centimeter));

        double kilometer = Double.valueOf(meterEditText.getText().toString()) / 1000;
        kilometerTextViewValue.setText(String.valueOf(kilometer));
    }
}