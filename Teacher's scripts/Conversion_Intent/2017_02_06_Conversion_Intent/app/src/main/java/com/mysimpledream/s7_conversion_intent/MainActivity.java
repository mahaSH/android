package com.mysimpledream.s7_conversion_intent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText editLastName;
    Button temperatureBtn, metricBtn, finishBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
    }

    private void initialize() {
        editLastName = findViewById(R.id.editLastName);
        temperatureBtn = findViewById(R.id.temperatureBtn);
        metricBtn = findViewById(R.id.metricBtn);
        finishBtn = findViewById(R.id.finishBtn);
    }

    public void operate (View view) {

        int btnID = view.getId();

        switch (btnID) {

            case R.id.temperatureBtn:
                temperatureConversation();
                break;
            case R.id.metricBtn:
                metricConversation();
                break;
            case R.id.finishBtn:
                finish();
        }
    }

    public void temperatureConversation() {

        Intent myIntent = new Intent(this, TemperatureActivity.class);
        startActivity(myIntent);
    }

    public void metricConversation() {

        // Read text from EditText
        String lastName = editLastName.getText().toString();

        //------------------------------------------------ Intent
        Intent myIntent = new Intent(this, MetricActivity.class);
        myIntent.putExtra("lastName", lastName);
        startActivity(myIntent);
    }
}
