package com.mysimpledream.s7_conversion_intent;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class TemperatureActivity extends AppCompatActivity {

    TextView tempratureTextViewValue_F;

    EditText temperatureEditText_C;
    Button returnBtn, convertBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temprature);

        initialize();
    }

    private void initialize() {
        temperatureEditText_C = findViewById(R.id.temperatureEditText_C);

        tempratureTextViewValue_F = findViewById(R.id.tempratureTextViewValue_F);

        convertBtn = findViewById(R.id.convertBtn);
        returnBtn = findViewById(R.id.returnBtn);
    }


    public void temperatureOperate(View view) {

        int btnID = view.getId();

        switch (btnID) {

            case R.id.convertBtn:
                buttonConvert();
                break;
            case R.id.returnBtn:
                finish();
                break;
        }
    }

    private void buttonConvert() {

        // Fahrenheit = (9.0/5.0) * Celsius + 32;
        float celsius = Float.valueOf(temperatureEditText_C.getText().toString());
        double result = ((9.0 / 5.0) * celsius ) + 32;
        tempratureTextViewValue_F.setText(String.valueOf(result));
    }
}